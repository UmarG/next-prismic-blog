webpackHotUpdate("static/development/pages/index.js",{

/***/ "./helpers/index.js":
/*!**************************!*\
  !*** ./helpers/index.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function linkResolver(doc) {
  if (doc.type === 'blog_post') {
    return "/blog/".concat(doc.uid);
  }

  return '/';
}

module.exports = {
  linkResolver: linkResolver
};

/***/ })

})
//# sourceMappingURL=index.js.e7f0c75c0a6500e035fd.hot-update.js.map